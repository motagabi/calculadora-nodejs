/*demo aula node JS - calculadora*/
/*Usando um pacote*/
console.log("/*Calculadora*/");
const calc = require("./calculadora/calculadora");

console.log("A:) -> " + calc.add(1, 1))
console.log("B:) -> " + calc.add(-123, 123123))
console.log("C:) -> " + calc.mul(8, 0))
console.log("D:) -> " + calc.mul(1239123, 12313))
console.log("E:) -> " + calc.div(123, -12))
console.log("F:) -> " + calc.div(313123, 0))


/*Exemplo simples de soma*/
const x = 1+1;
console.log("O valor de 1+1 são:", x);

/*FileSystem*/
const fs = require("fs");

fs.mkdir("./testeDir", function (){
    console.log("Criei o diretório testeDir");
});

fs.writeFile("./testeDir/abcd.txt","Ola tudo bem?", function(){
    console.log("Criei um arquivo");
});

fs.readFile("./testeDir/abcd.txt", function(err, data ){
    console.log("O meu arquivo possui dados: ", data.toString());
});

/*Utilizando o moment - pacote do nmp install $ npm i moment*/
const moment = require("moment");
console.log(moment().format());
console.log(moment().format("dddd"));

const myFunction = function(text){
    console.log(text);
}
function run(f){
    console.log("Eu bla bla");
    f("OI");
}
run(myFunction);
