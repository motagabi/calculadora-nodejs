const isNumber = require('is-number');

function subtracao(n1 , n2){
    if(isNumber(n1) && isNumber(n2)){
       return n1 - n2;
    }else{
        console.log("Digite dois valores numéricos!")
        return Number.isNaN;
    }
}

function adicao (n1, n2){
    if(isNumber(n1) && isNumber(n2)){
        return n1 + n2;
     }else{
         console.log("Digite dois valores numéricos!")
         return Number.isNaN;
     }
}
function multiplicacao (n1, n2){
    if(isNumber(n1) && isNumber(n2)){
        return n1 * n2;
     }else{
        console.log("Digite dois valores numéricos!");
        return Number.isNaN;
     }
}
function divisao (n1, n2){
    if(isNumber(n1) && isNumber(n2)){
        if(n2==0){
            return n1 / n2;
        }else{
            console.log("Não é possível dividir por zero")
            return Number.isNaN;   
        }
     }else{
         console.log("Digite dois valores numéricos!")
         return Number.isNaN;
     }
}
module.exports = {
    sub: subtracao,
    add: adicao,
    mul: multiplicacao,
    div: divisao
}